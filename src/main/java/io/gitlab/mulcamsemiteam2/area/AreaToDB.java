package io.gitlab.mulcamsemiteam2.area;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class AreaToDB {
    public static void main(String[] args) {
        String[][] sidoCodeAndFiles = new String[][] {
                // 시도코드, 파일명
                {"11", "entrc_seoul.txt"},
                {"21", "entrc_busan.txt"},
                {"22", "entrc_daegu.txt"},
                {"23", "entrc_incheon.txt"},
                {"24", "entrc_gwangju.txt"},
                {"25", "entrc_daejeon.txt"},
                {"26", "entrc_ulsan.txt"},
                {"29", "entrc_sejong.txt"},
                {"31", "entrc_gyunggi.txt"},
                {"32", "entrc_gangwon.txt"},
                {"33", "entrc_chungbuk.txt"},
                {"34", "entrc_chungnam.txt"},
                {"35", "entrc_jeonbuk.txt"},
                {"36", "entrc_jeonnam.txt"},
                {"37", "entrc_gyeongbuk.txt"},
                {"38", "entrc_gyeongnam.txt"},
                {"39", "entrc_jeju.txt"},
        };

        String dbUrl = "jdbc:mysql://localhost:3306/dbname";
        String dbUser = "user";
        String dbPwd = "pwd";

        Connection conn = null;
        PreparedStatement st = null;
        String line = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
            System.out.println("DB 연결 성공");

            // 트랜젝션 사용을 위해 auto commit을 끈다.
            conn.setAutoCommit(false);

            st = conn.prepareStatement(
                    "INSERT INTO area_all_doro_juso " +
                    "(" +
                    "sido_code," +
                    "sigungu_code," +
                    "entrance_code," +
                    "dong_code," +
                    "sido_name," +
                    "sigungu_name," +
                    "eupmyundong_name," +
                    "doro_code," +
                    "doro_name," +
                    "is_jiha," +
                    "building_main_num," +
                    "building_sub_num," +
                    "building_name," +
                    "zip_code," +
                    "building_classification," +
                    "is_building_gun," +
                    "guanhaldong," +
                    "axis_x," +
                    "axis_y" +
                    ") " +
                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            for (String[] sidoCodeAndFile : sidoCodeAndFiles) {
                String sidoCode = sidoCodeAndFile[0];
                String fileName = sidoCodeAndFile[1];

                System.out.printf("%s - %s 파일 파싱중\n", sidoCode, fileName);

                // 파일 하나씩 트랜젝션 처리
                try {
                    File dataFile = new File("area_data/" + fileName);

                    // charset을 파일과 제대로 안 맞춰주면 라인을 인식을 못 한다.
                    Scanner scan = new Scanner(dataFile, "EUC-KR");
                    while(scan.hasNextLine()){
                        line = scan.nextLine();
                        String[] lineParts = line.split("\\|", -1);

                        // 건물부번은 key니까 건물 부번까지 길이가 안되는 경우에는 다음 항목으로 패스
                        if(lineParts.length < 11) {
                            continue;
                        }
                        String 시군구코드 = lineParts[0];
                        String 출입구일련번호 = lineParts[1];
                        String 법정동코드 = lineParts[2];
                        String 시도명 = lineParts[3];
                        String 시군구명 = lineParts[4];
                        String 읍면동명 = lineParts[5];
                        String 도로명코드 = lineParts[6];
                        String 도로명 = lineParts[7];
                        boolean 지하여부 = lineParts[8].equals("1");
                        String 건물본번 = lineParts[9];
                        String 건물부번 = lineParts[10];

                        // 여기서부터는 필수 항목들이 아니니 ArrayIndexOutOfBoundsException가 뜨는 파트까지 처리하도록 하자.
                        String 건물명 = "";
                        String 우편번호 = "";
                        String 건물용도분류 = "";
                        boolean 건물군여부 = false;
                        String 관할행정동 = "";
                        Double X좌표 = null;
                        Double Y좌표 = null;

                        try {
                            건물명 = lineParts[11];
                            우편번호 = lineParts[12];
                            건물용도분류 = lineParts[13];
                            건물군여부 = lineParts[14].equals("1");
                            관할행정동 = lineParts[15];

                            // 좌표는 하나라도 뻑나면 둘다 null로 설정
                            try{
                                X좌표 = Double.parseDouble(lineParts[16]);
                                Y좌표 = Double.parseDouble(lineParts[17]);
                            } catch (NumberFormatException ignore) {
                                X좌표 = null;
                                Y좌표 = null;
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            // 무시
                        }

                        st.setString(1, sidoCode);
                        st.setString(2, 시군구코드);
                        st.setString(3, 출입구일련번호);
                        st.setString(4, 법정동코드);
                        st.setString(5, 시도명);
                        st.setString(6, 시군구명);
                        st.setString(7, 읍면동명);
                        st.setString(8, 도로명코드);
                        st.setString(9, 도로명);
                        st.setBoolean(10, 지하여부);
                        st.setString(11, 건물본번);
                        st.setString(12, 건물부번);
                        st.setString(13, 건물명);
                        st.setString(14, 우편번호);
                        st.setString(15, 건물용도분류);
                        st.setBoolean(16, 건물군여부);
                        st.setString(17, 관할행정동);
                        st.setObject(18, X좌표, java.sql.Types.DOUBLE);
                        st.setObject(19, Y좌표, java.sql.Types.DOUBLE);

                        st.execute();
                    }
                    scan.close();

                    // 파일이 하나마다 정상적으로 완료되면 커밋
                    conn.commit();
                } catch (Exception e) {
                    conn.rollback();

                    System.out.println("에러발생:\n" + line);
                    e.printStackTrace();

                    System.out.println(fileName + " 파일에서 에러가 발생되어 이 파일에 대한 처리는 롤백되었습니다.");
                    System.out.print("다음 파일을 이어서 파싱하시겠습니까?(Y/N): ");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String userInput = "";
                    try {
                        userInput = reader.readLine();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (!userInput.equals("Y") && !userInput.equals("y")) {
                        break;
                    }
                }

                System.out.println();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("DB 연결 실패");
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                    st = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
