/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `area_all_doro_juso` (
  `sido_code` varchar(2) COLLATE utf8mb4_general_ci NOT NULL COMMENT '시/도 코드',
  `sigungu_code` varchar(5) COLLATE utf8mb4_general_ci NOT NULL COMMENT '시/군/구 코드',
  `entrance_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL COMMENT '출입구 코드',
  `dong_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL COMMENT '법정동 코드',
  `sido_name` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '시/도 이름',
  `sigungu_name` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '시/군/구 이름',
  `eupmyundong_name` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '읍/면/동 이름',
  `doro_code` varchar(12) COLLATE utf8mb4_general_ci NOT NULL COMMENT '도로명 코드',
  `doro_name` varchar(80) COLLATE utf8mb4_general_ci NOT NULL COMMENT '도로명',
  `is_jiha` tinyint(1) NOT NULL COMMENT '지하 여부',
  `building_main_num` smallint NOT NULL COMMENT '건물 본번',
  `building_sub_num` smallint NOT NULL COMMENT '건물 부번',
  `building_name` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '건물 이름',
  `zip_code` varchar(5) COLLATE utf8mb4_general_ci NOT NULL COMMENT '우편번호',
  `building_classification` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '건물 용도 분류',
  `is_building_gun` tinyint(1) NOT NULL COMMENT '건물군 여부',
  `guanhaldong` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '관할 행정동 이름',
  `axis_x` double DEFAULT NULL COMMENT 'x좌표',
  `axis_y` double DEFAULT NULL COMMENT 'y좌표',
  PRIMARY KEY (`doro_code`,`is_jiha`,`building_main_num`,`building_sub_num`,`dong_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='위치정보요약DB https://www.juso.go.kr/addrlink/addressBuildDevNew.do?menu=geodata';

DELETE FROM `area_all_doro_juso`;
/*!40000 ALTER TABLE `area_all_doro_juso` DISABLE KEYS */;
/*!40000 ALTER TABLE `area_all_doro_juso` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
